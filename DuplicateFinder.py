#!/usr/bin/python3.7

import logging
import time

from SQLConnector import SQLConnector


class DuplicateFinder:
    def __init__(self, connector):
        self._connector = connector

    def find_duplicates(self):
        start = time.time()

        dups = []

        logging.debug("Finding duplicates")

        dup_checksums = self._connector.find_duplicate_checksums()

        end = time.time()
        duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
        logging.info(f"Finding duplicates query latency: {duration}")

        logging.debug("Outputting duplicates")

        if dup_checksums is not None:
            for checksum in dup_checksums:
                filesize, dup_files = self._connector.get_filenames(checksum)
                dups.append({'files':dup_files, 'filesize':filesize})
        end = time.time()
        duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
        logging.info(f"Finding duplicates latency: {duration}")

        logging.debug("Finding duplicates completed")

        return dups

    def __del__(self):
        pass
