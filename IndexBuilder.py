#!/usr/bin/python3.7

import glob
import hashlib
import io
import logging
import os
import re
from subprocess import  check_output, CalledProcessError, STDOUT
import time

from ThreadPool import ThreadPool, FileWorker


def get_duration_in_s(filename):
    
    duration_in_s = 0

    # TODO: Use ctypes to call without using a separate process

    # Copied from https://stackoverflow.com/questions/3844430/how-to-get-the-duration-of-a-video-in-python
    command = [
        'ffprobe', 
        '-v', 
        'quiet', 
        '-show_entries', 
        'format=duration', 
        '-of', 
        'default=noprint_wrappers=1:nokey=1', 
        filename
    ]

    try:
        duration_in_s = check_output( command, stderr=STDOUT ).decode()
        # 20190704: with "-v quiet" instead of "-v error" above, the extra garbage -errors- aren't displayed
        # There may be random garbage at the end
        # duration_in_s = re.match('\d+\.\d+', duration_in_s).group(0)
    except CalledProcessError as e:
        # If there's an error, just assume it's not a video
        #print("ffprobe failed: {0}".format(e))
        duration_in_s = 0

    return int(float(duration_in_s))


def win_path(path):
    match = re.match('(/(cygdrive/)?)(.*)', path)
    if not match:
        return path.replace('/', '\\')
    dirs = match.group(3).split('/')
    dirs[0] = f'{dirs[0].upper()}:'
    return '\\'.join(dirs)


class IndexBuilder:
    ''' Iterates through the files and adds entries. '''

    def __init__(self, nochecksum):
        self._nochecksum = nochecksum
        self._counter = 0

    def index(self, paths):
        logging.debug("Indexing started")
        start = time.time()

        self._counter = 0

        num_threads = os.cpu_count()
        #num_threads = 1
        #num_threads = 20
        pool = ThreadPool(num_threads, FileWorker)
        logging.debug(f"Using {num_threads} threads")

        for path in paths:
            regex = path + '/**'
            #files = glob.glob(regex, recursive=True)
            #logging.debug(f"Processing {num_files} files")
            #pool.map(builder, files)
            #pool.map(builder, glob.iglob(regex, recursive=True))
            pool.map(self, glob.iglob(regex, recursive=True))
            pool.wait_for_completion()
        end = time.time()
        duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
        logging.debug("Indexing completed")
        logging.info(f"Indexing latency: {duration}")

    def add_file(self, connector, filename):
        md5checksum = None
        filesize = None
        logging.debug(filename)

        if os.path.isfile(filename) is False:
            return

        filename = os.path.abspath(filename)
        #simple_filename = os.path.basename(filename)
        #directoryname = os.path.abspath(os.path.dirname(filename))

        # Want format: 'YYYY-MM-DD HH:MM:SS'
        modifiedDate = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(os.path.getmtime(filename)))

        # If there's already an entry for filename and modifiedData, just return
        if connector.file_exists(filename, modifiedDate) == False:
            #print("Processing a file: {0}".format(filename))

            filesize = os.path.getsize(filename)

            if self._nochecksum:
                md5checksum = 0
            else:
                try:
                    md5 = hashlib.md5()
                    # mmap() is slower than just using fd.read()
    #                with open(filename, mode="rb") as fd:
    #                    map = mmap.mmap(fd.fileno(), 0, prot=mmap.PROT_READ)
    #                    md5.update(map.read())
                    with io.open(filename, mode="rb") as fd:
                        content = fd.read()
                        md5.update(content)
                    # reading 1mb at a time is slower with smaller files
    #                with io.open(filename, mode="rb") as fd:
    #                    content = fd.read(1024*1024)
    #                    while content:
    #                        md5.update(content)
    #                        content = fd.read(1024*1024)
                    md5checksum = md5.hexdigest()
    #                md5checksum = hashlib.md5(open(filename, 'rb').read()).hexdigest()
                except Exception as e:
                    logging.error(f"MD5 failed for: {filename} exception: {e}")

            # Rather than use the extension or analyzing the file to determine if it's a video,
            # use ffmpeg and whether the Duration > 0.0, which will prevent the need to 'intelligently'
            # detect videos and eliminates any that are corrupted.
            duration_in_s = get_duration_in_s(win_path(filename))
            lastrowid = 0

            if duration_in_s > 0:
                density = int(filesize / duration_in_s)

                lastrowid = connector.add_video(density, duration_in_s)

            if md5checksum is not None:
                connector.add_file(filename, md5checksum, modifiedDate, filesize, lastrowid)
            #print(f"Added {filename}")

        else:
            pass


    def __del__(self):
        pass
