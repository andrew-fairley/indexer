#!/usr/bin/python3.7

from enum import Enum
import logging
import os
import sqlite3
from sqlite3 import Error
import time


class State(Enum):
    CONNECTED = 1
    DISCONNECTED = 2


class SQLConnector:
    ''' Handles the connection to a SQL database. '''
    def __init__(self):
        self._state = State.DISCONNECTED
        self._connection = None

        self._SQL_CREATE_TABLE_FILES = ''' CREATE TABLE IF NOT EXISTS files (
                                            id integer PRIMARY KEY,
                                            filename text NOT NULL UNIQUE COLLATE NOCASE,
                                            md5checksum text NOT NULL,
                                            modifiedDate DATETIME,
                                            filesize integer,
                                            video_id integer
                                            ); '''

        self._SQL_CREATE_TABLE_VIDEOS = ''' CREATE TABLE IF NOT EXISTS videos (
                                            id integer PRIMARY KEY,
                                            density int,
                                            duration_in_s int
                                            ); '''

        self._SQL_INSERT_TABLE_FILES = ''' INSERT OR REPLACE INTO files 
                                            (filename, md5checksum, modifiedDate, filesize, video_id) 
                                            VALUES ("{filename}", "{md5checksum}", "{modifiedDate}", "{filesize}", "{video_id}"); '''

        self._SQL_INSERT_TABLE_VIDEOS = ''' INSERT OR REPLACE INTO videos 
                                            (density, duration_in_s) 
                                            VALUES ("{density}", "{duration_in_s}"); '''

#        self._SQL_EXISTS_FILE = ''' SELECT count(*) FROM files WHERE filename = "{filename}" COLLATE NOCASE AND modifiedDate = "{modifiedDate}"; '''
        self._SQL_EXISTS_FILE = ''' SELECT count(*) FROM files WHERE filename = "{filename}" AND modifiedDate = "{modifiedDate}"; '''

        self._SQL_GET_VIDEO_DENSITIES = ''' SELECT density, filename, filesize 
                                            FROM files 
                                            INNER JOIN videos on videos.id = files.video_id 
                                            WHERE video_id != 0
                                            ORDER BY density DESC; '''

        self._SQL_GET_FILES_BY_CHECKSUM = ''' SELECT filename, filesize FROM files WHERE md5checksum = "{md5checksum}"; '''

        self._SQL_MD5CHECKSUM_INDEX = ''' CREATE INDEX IF NOT EXISTS md5checksumindex ON files(md5checksum); '''

        self._SQL_FILENAME_INDEX = ''' CREATE INDEX IF NOT EXISTS filenameindex ON files(filename); '''

        self._SQL_VIDEO_INDEX = ''' CREATE INDEX IF NOT EXISTS videoindex ON files(video_id); '''

        # Original - but very slow (several minutes or more)
        #self._SQL_QUERY_CHECKSUM_GROUPS = ''' SELECT md5checksum, COUNT(*) FROM files GROUP BY md5checksum HAVING COUNT(*) > 1 ORDER BY filesize DESC; '''

        # New, very fast (seconds)
        self._SQL_QUERY_CHECKSUM_GROUPS = ''' SELECT DISTINCT md5checksum
                                                FROM files 
                                                WHERE md5checksum IN (
                                                    SELECT md5checksum 
                                                    FROM files 
                                                    GROUP BY md5checksum 
                                                    HAVING COUNT(md5checksum) > 1) 
                                                ORDER BY filesize DESC; '''


        self._SQL_QUERY_FILENAMES = ''' SELECT filename, video_id FROM files; '''

        #self._SQL_DELETE_FILE = ''' DELETE FROM files WHERE filename="{filename}" COLLATE NOCASE; '''
        self._SQL_DELETE_FILE = ''' DELETE FROM files WHERE filename="{filename}"; '''

        self._SQL_DELETE_VIDEO = ''' DELETE FROM videos WHERE id="{video_id}"; '''

    def open_connection(self):
        if self._state == State.CONNECTED:
            return

        # Attempt to open a database named 'index.db' in the current directory
        filename = "./index.db"

        #logging.debug(f"Opening {filename}")

        try:
            # Will connect if it exists, and create it if it doesn't
            self._connection = sqlite3.connect(filename, check_same_thread=False)
        except Exception:
            logging.error(f"Failed to open or create the database {filename}")

        c = self._connection.cursor()

        c.execute("PRAGMA foreign_keys = ON;")

        # Default cache size is -2000 or 2MB, this will increase it to 6MB
        c.execute("PRAGMA cache_size = -20000;")

        # Store temporary tables in memory to speed up indices
        c.execute("PRAGMA temp_store = MEMORY;")

        # Don't sync to disk after each write - no perf improvement
        c.execute("PRAGMA synchronous = OFF;")

        # Don't sync to WAL each time
        #c.execute("PRAGMA synchronous = NORMAL;")

        # Use an in-memory rollback journal - ~2% faster
        c.execute("PRAGMA journal_mode = MEMORY;")

        # Use write-ahead logging - it's ~30% faster to not use WAL
        #c.execute("PRAGMA journal_mode = WAL;")

        self._state = State.CONNECTED

        #logging.debug("Opening completed")

    def init_database(self):
        if self._state == State.DISCONNECTED:
            raise Exception("init_database() should not be called if the db connection hasn't been made")

        c = self._connection.cursor()

        c.execute("PRAGMA foreign_keys = ON;")

        # Create the Videos table if it doesn't already exist
        try:
            c.execute(self._SQL_CREATE_TABLE_VIDEOS)
        except Exception as e:
            logging.error(f"Failed to create Videos table: {e}")

        # Create the Files table if it doesn't already exist
        try:
            c.execute(self._SQL_CREATE_TABLE_FILES)
        except Exception as e:
            logging.error(f"Failed to create Files table: {e}")

        # Create INDEX based on files's md5checksum
        c.execute(self._SQL_MD5CHECKSUM_INDEX)
        # Create INDEX based on files' filename
        c.execute(self._SQL_FILENAME_INDEX)
        c.execute(self._SQL_VIDEO_INDEX)


    def add_file(self, filename, md5checksum, modifiedDate, filesize, video_id):
        if self._state == State.DISCONNECTED:
            raise Exception("add_file() should not be called if the db connection hasn't been made")

        try:
            insertStmnt = self._SQL_INSERT_TABLE_FILES.format(filename=filename,
                                                                md5checksum=md5checksum,
                                                                modifiedDate=modifiedDate,
                                                                filesize=filesize,
                                                                video_id=video_id)
            c = self._connection.cursor()
            #print(insertStmnt)
            c.execute(insertStmnt)
        except Exception as e:
            logging.error(f"Failed to insert to files table: {filename} error: {e}")


    def add_video(self, density, duration_in_s):
        if self._state == State.DISCONNECTED:
            raise Exception("add_video() should not be called if the db connection hasn't been made")

        lastrowid = 0
        try:
            insertStmnt = self._SQL_INSERT_TABLE_VIDEOS.format(density=density,
                                                               duration_in_s=duration_in_s)
            c = self._connection.cursor()
            #print(insertStmnt)
            lastrowid = c.execute(insertStmnt).lastrowid
        except Exception as e:
            logging.error(f"Failed to insert to videos table: error: {e}")
        return lastrowid


    def file_exists(self, filename, modifiedDate):
        if self._state == State.DISCONNECTED:
            raise Exception("file_exists() should not be called if the db connection hasn't been made")

        try:
            existsStmnt = self._SQL_EXISTS_FILE.format(filename=filename, modifiedDate=modifiedDate)
            #print(existsStmnt)
            c = self._connection.cursor()
            c.execute(existsStmnt)
            data = c.fetchone()[0]
            return data != 0
        except Exception as e:
            # This log message triggered an exception when running with multiple threads so it was disabled
            logging.error(f"Failed to query for filename: {filename} exception: {e}")
            #pass


    def find_duplicate_checksums(self):
        if self._state == State.DISCONNECTED:
            raise Exception("find_duplicate_checksums() should not be called if the db connection hasn't been made")

        dups = []
        try:
            c = self._connection.cursor()
            c.execute(self._SQL_QUERY_CHECKSUM_GROUPS)
            all_rows = c.fetchall()
            for entry in all_rows:
                dups.append(entry[0])
                #print(entry[0])
        except Exception as e:
            logging.error(f"Failed to query for duplicates: {e}")

        return dups


    def cleanup_old_entries(self):
        if self._state == State.DISCONNECTED:
            raise Exception("cleanup_old_entries() should not be called if the db connection hasn't been made")

        start = time.time()
        logging.debug("Cleanup started")
        num_files = 0
        try:
            c = self._connection.cursor()
            c.execute(self._SQL_QUERY_FILENAMES)
            # TODO: Speed this up by using a thread pool
            for row in c:
                if os.path.isfile(row[0]) == False:
                    num_files += 1
                    filename = row[0]
                    # This log message triggered an exception when running with multiple threads so it was disabled
                    #logging.debug(f'Deleting {filename}')
                    # Remove the files entry
                    deleteStmnt = self._SQL_DELETE_FILE.format(filename=row[0])
                    cur = self._connection.cursor()
                    cur.execute(deleteStmnt)

                    # Remove the videos entry
                    deleteStmnt = self._SQL_DELETE_VIDEO.format(video_id=row[1])
                    cur = self._connection.cursor()
                    cur.execute(deleteStmnt)
            self.commit_transaction()
            # Cleanup the db
            c.execute("VACUUM")
        except Exception as e:
            logging.warning(f"Failed to cleanup old entries: {e}")
        end = time.time()
        duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
        logging.info(f"Cleaned up {num_files} files")
        logging.info(f"Cleanup latency: {duration}")


    def get_filenames(self, checksum):
        if self._state == State.DISCONNECTED:
            raise Exception("get_filenames() should not be called if the db connection hasn't been made")

        files = []
        filesize = 0
        try:
            searchStmnt = self._SQL_GET_FILES_BY_CHECKSUM.format(md5checksum=checksum)
            c = self._connection.cursor()
            c.execute(searchStmnt)
            all_rows = c.fetchall()
            for entry in all_rows:
                files.append(entry[0])
                if filesize == 0:
                    filesize = entry[1]
                #print(entry[0])
        except Exception as e:
            logging.error(f"Failed to query for filenames by checksum: {e}")

        return filesize, files


    def get_file_densities(self):
        if self._state == State.DISCONNECTED:
            raise Exception("get_file_destinies() should not be called if the db connection hasn't been made")

        files = []
        try:
            searchStmnt = self._SQL_GET_VIDEO_DENSITIES
            c = self._connection.cursor()
            c.execute(searchStmnt)
            all_rows = c.fetchall()
            for entry in all_rows:
                files.append(entry)
        except Exception as e:
            logging.error(f"Failed to query for filenames by checksum: {e}")

        return files


    def commit_transaction(self):
        if self._state == State.DISCONNECTED:
            raise Exception("commit_transaction() should not be called if the db connection hasn't been made")

        try:
            if self._connection is not None:
                self._connection.commit()
        except Exception as e:
            logging.error(f"commit_changes() failed with error {e}")


    def rollback(self):
        if self._state == State.DISCONNECTED:
            raise Exception("rollback() should not be called if the db connection hasn't been made")

        try:
            if self._connection is not None:
                self._connection.execute("rollback")
        except Exception as e:
            logging.error(f"commit_changes() failed with error {e}")


    def close_connection(self):
        if self._state == State.DISCONNECTED:
            return

        # Close any opened connection
        try:
            if self._connection is not None:
                self._connection.close()
        except Exception:
            pass
        self._state = State.DISCONNECTED


    def __del__(self):
        self.close_connection()
