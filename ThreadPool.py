#!/usr/bin/python3.7

import logging
from queue import Queue
from multiprocessing import JoinableQueue
from threading import Thread
from SQLConnector import SQLConnector


class FileWorker(Thread):
    """ Thread executing tasks from a given tasks queue """
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

        self._connector = SQLConnector()
        self._connector.open_connection()

    def run(self):
        while True:
            builder, filename = self.tasks.get()
            try:
                # TODO: Update use of Queue so that the function 'add_file' can be passed in as a parameter
                builder.add_file(self._connector, filename)
                self._connector.commit_transaction()
            except Exception as e:
                # An exception happened in this thread
                print(e)
            finally:
                # Commit everything the thread has done
                #self._connector.commit_transaction()
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()
    
    def __del__(self):
        try:
            self._connector.close_connection()
        except:
            pass


class ThreadPool:
    """ Pool of threads consuming tasks from a queue """
    def __init__(self, num_threads, worker):
        #self.tasks = Queue(num_threads)
        self.tasks = JoinableQueue(num_threads)
        for _ in range(num_threads):
            worker(self.tasks)

    def add_task(self, builder, filename):
        """ Add a task to the queue """
        self.tasks.put((builder, filename))

    def map(self, builder, files_list):
        """ Add a list of tasks to the queue """
        for filename in files_list:
            self.add_task(builder, filename)

    def wait_for_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()
