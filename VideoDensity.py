#!/usr/bin/python3.7

import logging
import time

from SQLConnector import SQLConnector


class VideoDensity:
    def __init__(self, connector):
        self._connector = connector
    
    def get_file_densities(self):
        start = time.time()

        # Return array of triplets
        metadata = self._connector.get_file_densities()

        end = time.time()
        duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
        logging.info(f"Getting densities latency: {duration}")

        logging.debug("Video densities completed")

        return metadata

    def __del__(self):
        pass
