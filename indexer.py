#!/usr/bin/python3.7

import argparse
import logging
import time

from DuplicateFinder import DuplicateFinder
from SQLConnector import SQLConnector
from IndexBuilder import IndexBuilder
from VideoDensity import VideoDensity


def main():
    start = time.time()

    parser = argparse.ArgumentParser(description="Duplicate file finder")

    parser.add_argument('-c', '--cleanup', action='store_true', dest='do_cleanup', default=False,
                        help='Cleanup old entries')
    parser.add_argument('-q', '--quiet', action='store_true', dest='quiet', default=False,
                        help='Only logs info and above if set')
    parser.add_argument('--nochecksum', action='store_true', dest='nochecksum', default=False,
                        help='If specified, will skip generating a checksum for each file')
    parser.add_argument('-u', '--update', action='append', dest='searchdirs', default=[],
                        help='Search for new duplicates')
    parser.add_argument('-r', '--report', action='store', nargs=1, choices=['dup', 'density'], dest='do_report', default='',
                        help='dup: Find and report duplicate files, density: report density of videos')

    results = parser.parse_args()

    # If no options are selected, show the help message
    if results.do_cleanup == False and len(results.searchdirs) == 0 and not results.do_report:
        parser.print_help()
        return

    # TODO: Register a custom logger
    logging.basicConfig()
    if results.quiet:
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.DEBUG)

    connector = SQLConnector()

    connector.open_connection()

    connector.init_database()

    if results.do_cleanup:
        connector.cleanup_old_entries()

    if len(results.searchdirs) > 0:
        builder = IndexBuilder(results.nochecksum)
        builder.index(results.searchdirs)

    for report_type in results.do_report:
        if report_type == 'dup':
            finder = DuplicateFinder(connector)
            dups = finder.find_duplicates()

            for group in dups:
                print(group['filesize'])
                for file in group['files']:
                    print(file)
                print('')
        elif report_type == 'density':
            aggregator = VideoDensity(connector)
            metadata = aggregator.get_file_densities()

            for entry in metadata:
                print("{0} {1} {2}".format(entry[0], entry[1], entry[2]).encode("utf-8").decode())

    connector.close_connection()

    end = time.time()
    duration = time.strftime("%H:%M:%S", time.gmtime(end - start))
    logging.info(f"Total latency: {duration}")


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logging.exception("**** Fatal error in main loop")
