# Description

Indexer can be used to:
- Find duplicates
- Report on the 'density' of videos

# Running

# Future

* TODO: Add a mode meant for density that doesn't generate an md5 checksum on files
* TODO: For case-insensitive searches, using a table with "COLLATE NOCASE" should be 6-10x faster then using a SELECT with "COLLATE NOCASE"
* TODO: Create a REST interface
* TODO: Create a front-end that uses the REST interface
    REST Interface:
   
    Get a list of duplicate file pairings (based on md5checksum)
    GET /duplicates/
    
    Get the file info for a specific pairing
    GET /duplicates/<md5checksum>/
 
    Delete a file (recycle bin)
    DELETE /files/<filename>/

    To delete files:
    import send2trash
    send2trash.send2trash(filename)


# Troubleshooting

## UnicodeEncodeError: 'charmap' codec can't encode - character maps to <undefined>

If you see this error when redirecting stdout, the problem isn't in Python but in your terminal (likely Windows PowerShell.)

To fix it:
1. Check your current terminal encoding:
```powershell
> echo $OutputEncoding
```
It will likely report us-anscii
2. Update your encoding by updating or creating the file $PROFILE:
```powershell
> echo $PROFILE
C:\Users\Andrew\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
```
to have the following lines:
```powershell
$OutputEncoding = New-Object -typename System.Text.UTF8Encoding
[Console]::OutputEncoding = New-Object -typename System.Text.UTF8Encoding
```
3. Close and reopen PowerShell to confirm the settings have taken effect:
```powershell
> echo $OutputEncoding
BodyName          : utf-8
EncodingName      : Unicode (UTF-8)
HeaderName        : utf-8
WebName           : utf-8
WindowsCodePage   : 1200
IsBrowserDisplay  : True
IsBrowserSave     : True
IsMailNewsDisplay : True
IsMailNewsSave    : True
IsSingleByte      : False
EncoderFallback   : System.Text.EncoderReplacementFallback
DecoderFallback   : System.Text.DecoderReplacementFallback
IsReadOnly        : True
CodePage          : 65001
```
